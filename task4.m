function [A_k S] = task4(image, k)
  A = double(imread(image));
    
  miu = mean(A,2);
  A = A .- miu;
  Z = A*(transpose(A)/(size(A,2)-1));
  
  [V S] = eig(Z);
  
  W = V(:,1:k);
  Y = transpose(W)*A;
  
  A_k = W*Y .+miu;
endfunction
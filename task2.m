function task2()
  %A = double(imread('in/images/image2.gif')); 
  A = double(imread('in/images/image3.gif')); 
  [U S V] = svd(A);
  
  subplot(2,2,1);
  plot(diag(S));
  
  k = [1:19 20:20:99 100:30:min(size(A))];
  info = [];
  for i = 1:size(k,2)
    info(i) = sum(diag(S(1:k(i),1:k(i)))) / sum(diag(S));
  end
  subplot(2,2,2);
  plot(k,info);
  
  info = [];
  for i = 1:size(k,2)
    %A_k = task1('in/images/image2.gif',k(i));
    A_k = task1('in/images/image3.gif',k(i));
    info(i) = sum(sum((A - A_k) .^2 )) / (size(A,1)*size(A,2));
  end
  subplot(2,2,3);
  plot(k,info);

  info = (size(A,1) * k + size(A,2) * k + k) / (size(A,1)*size(A,2));
  subplot(2,2,4);
  plot(k,info);
end
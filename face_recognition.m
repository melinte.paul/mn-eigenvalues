function [min_dist output_img_index] = face_recognition(image_path, m, A, eigenfaces, pr_img)
  image = double(rgb2gray(imread(image_path)));
  image = reshape(image',prod(size(image)),1);

  image_normalized = image-m;
  
  pr_test_img = eigenfaces' * image_normalized;
  sqrt(sum((pr_img-pr_test_img) .^2 , 1))
  [min_dist output_img_index] = min(sqrt(sum((pr_img-pr_test_img) .^2 , 1)));
endfunction

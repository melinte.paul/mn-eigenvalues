function [A_k S] = task3(image, k)
  A = double(imread(image));
  
  miu = mean(A,2);
  A = A .- miu;
  
  Z = A'/sqrt(size(A,2));
  [U S V] = svd(Z);
  
  W = V(:,1:k);
  Y = W'*A;
  
  A_k = W*Y .+ miu;
endfunction
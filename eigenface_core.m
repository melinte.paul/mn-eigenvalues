function [m A eigenfaces pr_img] = eigenface_core(database_path)  
  T = [];
  for i = 1:10
    image = double(rgb2gray(imread(strcat(database_path,'/',num2str(i),'.jpg'))));
    image = reshape(image',prod(size(image)),1);
    T = [T image];
  endfor

  m = mean(T,2);
  
  A = T - m;
  
  [V l] = eig(A' * A);
    
    
  for i=1:size(l,1)
    if(l(i,i)<1)
      V(:,i) = zeros(size(V(:,i)));
    endif
  endfor  
  eigenfaces = A*V;
  pr_img = eigenfaces' * A;
endfunction
